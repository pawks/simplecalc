%{
 
#include <stdio.h>
#include "y.tab.h"
int c;
extern int yylval;
%}
%start comment
%%
<comment>[^\n]    ;
<comment>"\n"     {
                    BEGIN INITIAL;
                  }
<INITIAL>"#"       {
                    BEGIN comment;
                  }
<INITIAL>" "       ;
<INITIAL>[a-z]     {
                    c = yytext[0];
                    yylval = c - 'a';
                    return(LETTER);
                  }
<INITIAL>[0-9]     {
                    c = yytext[0];
                    yylval = c - '0';
                    return(DIGIT);
                  }
<INITIAL>[^a-z0-9\b#]     {
                          c = yytext[0];
                          return(c);
                        }

                        