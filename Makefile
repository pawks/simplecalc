lex:=calc
yacc:=calc
infile:=input
output:=output
outfile:=compiler
macros:=-D STDIN
dir:=~/iiitdm_work/lexnyacc
default:full_clean build link execute
default_link:full_clean build link_input execute_input
default_link_output:full_clean build link_input execute_input_output


.PHONY: full_clean
full_clean:
	rm -rf *.out lex.yy.c y.tab.c y.tab.h $(output).txt

.PHONY: build
build:
	@echo building...
	@echo generating intermediate yacc files
	yacc -d $(dir)/$(yacc).yacc
	@echo generating intermediate lex files
	lex $(dir)/$(lex).lex
	@echo building finished...

.PHONY: link_input
link_input:
	@echo linking with input...
	cc y.tab.c lex.yy.c $(macros) -w -o $(dir)/$(outfile).out
	rm -rf y.tab.c y.tab.h lex.yy.c
	@echo linking finished...

.PHONY: link
link:
	@echo linking...
	cc y.tab.c lex.yy.c -w -o $(dir)/$(outfile).out
	rm -rf y.tab.c y.tab.h lex.yy.c
	@echo linking finished...

.PHONY: execute
execute:
	@echo executing...
	./$(outfile).out

.PHONY: execute_input
execute_input:
	@echo executing with input...
	./$(outfile).out $(infile).txt

.PHONY: execute_input_output
execute_input_output:
	@echo executing with input...
	./$(outfile).out $(infile).txt >> $(dir)/$(output).txt

#make default_link_output macros="-D I"
#make default_link macros="-D I"
#make